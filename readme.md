$ git -v
$ docker -v
$ docker-compose -v
$ git clone git@gitlab.com:dariushine/lynxed-dev.git && cd lynxed-dev
$ git submodule init && git submodule update
$ git submodule foreach npm install
$ docker-compose up -d

docker exec -ti <container image> npx knex:migrate